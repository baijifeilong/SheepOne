using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BestHTTP;
using DG.Tweening;
using EasyUI.Toast;
using Newtonsoft.Json;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

[Serializable]
public class Team
{
    public string name;
    public string leader;
    public float time;
    public int done;
    public float average;
}

public class GameManager : MonoBehaviour
{
    public GameObject cardPrefab;
    private readonly Vector3 _fenceZeroPosition = new Vector3(-3, -4.25f, 0);
    private readonly Vector3 _undoZeroPosition = new Vector3(-3, -2.75f, 0);
    public static GameManager Instance;
    public List<Sprite> cardSprites;
    private List<Card> _fenceCards = new();
    private readonly List<Card> _allCards = new();
    private List<int> _values;
    private AudioSource _audioSource;
    public AudioClip collectingClip;
    public AudioClip dismissingClip;
    public List<GameObject> sheepList;
    public Button backButton;
    public Button undoButton;
    public Button shuffleButton;
    public Text timeText;
    private float _timeUsed;
    private float _minTime;
    private float _todayMinTime;
    private bool _done;
    public GameObject menuPanel;
    public GameObject rankPanel;
    public GameObject personPanel;
    public List<Team> teams;
    public bool paused;
    public TextAsset areaTextAsset;
    public TextAsset charNameAsset;
    public Dictionary<string, List<string>> AreaDict;
    public List<string> charNames;
    public bool uploaded;

    // private const string ApiPrefix = "http://localhost:5000";
    private const string ApiPrefix = "https://sheep.pinkmem.com";
    private Text _successTimeText;
    private Text _failureTimeText;
    public GameObject successPanel;
    public GameObject failurePanel;
    private bool _cheat;

    private void Awake()
    {
        Instance = this;
        Application.targetFrameRate = 60;
        AreaDict = JsonConvert.DeserializeObject<Dictionary<string, List<string>>>(areaTextAsset.text);
        charNames = charNameAsset.text.Split("\r\n").ToList();
    }

    private List<int> GenerateRandomValue(int count)
    {
        return Enumerable.Range(0, count / 3).Select(_ => Random.Range(0, cardSprites.Count))
            .Select(x => Enumerable.Repeat(x, 3)).SelectMany(x => x).OrderBy(_ => Guid.NewGuid()).ToList();
    }

    private Card GenerateCard()
    {
        var cardIndex = _allCards.Count;
        var cardObject = Instantiate(cardPrefab, Vector3.zero, Quaternion.identity, transform);
        var spriteRenderer = cardObject.GetComponentsInChildren<SpriteRenderer>().Last();
        var valueIndex = _values[cardIndex];
        spriteRenderer.sprite = cardSprites[valueIndex];
        cardObject.GetComponent<SpriteRenderer>().sortingOrder = cardIndex * 10 + 1;
        spriteRenderer.sortingOrder = cardIndex * 10 + 2;
        var card = cardObject.GetComponent<Card>();
        card.value = valueIndex + 1;
        _allCards.Add(card);
        return card;
    }

    private bool _rankRefreshing;

    public void RefreshRank()
    {
        if (_rankRefreshing) return;
        Debug.Log("Refreshing...");
        _rankRefreshing = true;
        var request = new HTTPRequest(new Uri(ApiPrefix + "/api/rank"),
            (req, rsp) =>
            {
                _rankRefreshing = false;
                if (req.State != HTTPRequestStates.Finished)
                {
                    Toast.Show("排名刷新失败.", Color.red);
                    return;
                }

                teams = JsonConvert.DeserializeObject<List<Team>>(rsp.DataAsText);
                Debug.Log(teams);
                rankPanel.GetComponent<RankPanel>().RefreshRank();
            });
        request.ConnectTimeout = TimeSpan.FromSeconds(10);
        request.Timeout = TimeSpan.FromSeconds(10);
        request.Send();
    }

    private void Start()
    {
        _cheat = PlayerPrefs.GetInt("cheat", 0) == 1;
        if (_cheat) cardSprites = cardSprites.GetRange(0, 3);

        _successTimeText = successPanel.transform.GetComponentsInChildren<Text>()[2];
        _failureTimeText = failurePanel.transform.GetComponentsInChildren<Text>()[2];
        _minTime = PlayerPrefs.GetFloat("minTime", 9999);
        _todayMinTime = PlayerPrefs.GetFloat("todayMinTime", 9999);
        _audioSource = GetComponent<AudioSource>();
        var count = 11 * 3 + 6 * 6 + 5 * 5 + 4 * 4 + 3 * 3 + 2 * 2;
        if (_cheat) count = 33;

        _values = GenerateRandomValue(count);
        Vector3 zeroPosition = new Vector3(-0.5f, 3.5f, 0);
        for (int floor = 0; floor < 5; floor++)
        {
            if (_cheat) break;
            var width = 2 + floor;
            for (int row = 0; row < width; row++)
            {
                for (int column = 0; column < width; column++)
                {
                    var card = GenerateCard();
                    var position = zeroPosition + new Vector3(column, -row, 7 - floor);
                    card.originalPosition = position;
                    card.transform.position = position;
                }
            }

            zeroPosition += new Vector3(-.5f, +.5f, 0);
        }

        var leftPosition = new Vector3(-3f, -1.5f, 15);
        var centerPosition = new Vector3(0, -1.5f, 15);
        var rightPosition = new Vector3(3f, -1.5f, 15);
        for (var i = 0; i < 11; i++)
        {
            var leftCard = GenerateCard();
            leftCard.originalPosition = leftPosition;
            leftCard.transform.position = leftPosition;
            var centerCard = GenerateCard();
            centerCard.originalPosition = centerPosition;
            centerCard.transform.position = centerPosition;
            var rightCard = GenerateCard();
            rightCard.originalPosition = rightPosition;
            rightCard.transform.position = rightPosition;
            leftPosition += new Vector3(0, .075f, -1);
            centerPosition += new Vector3(0, .075f, -1);
            rightPosition += new Vector3(0, .075f, -1);
        }
    }

    private void RefreshTodayScore()
    {
        var lastPlayDate = PlayerPrefs.GetString("lastPlayDate", "0000-00-00");
        var todayDate = DateTime.Now.ToString("yyyy-MM-dd");
        if (todayDate != lastPlayDate)
        {
            PlayerPrefs.SetString("lastPlayDate", todayDate);
            PlayerPrefs.SetFloat("todayMinTime", 9999.99f);
        }
    }

    public void UploadScore()
    {
        if (Instance.uploaded)
        {
            Toast.Show("最新成绩已上传");
            return;
        }

        var provinces = AreaDict.Keys.ToList();
        var areas = AreaDict[provinces[PlayerPrefs.GetInt("teamIndex", 0)]];
        var team = provinces[PlayerPrefs.GetInt("teamIndex", 0)];
        var region = areas[PlayerPrefs.GetInt("regionIndex", 0)];
        var theName = charNames[PlayerPrefs.GetInt("nameIndex", 0)];
        Debug.Log("Uploading...");
        var request = new HTTPRequest(new Uri(ApiPrefix + "/api/upload"), HTTPMethods.Post, (req, _) =>
        {
            if (req.State != HTTPRequestStates.Finished)
            {
                Toast.Show("上传成绩失败.", Color.red);
                return;
            }

            Instance.uploaded = true;
            Toast.Show("上传成绩成功.", Color.green);
        });
        var teamInfo = new Team();
        var latestTime = PlayerPrefs.GetFloat("latestTime", 9999.99f);
        var simpleRegion = region.Split("-")[1];
        teamInfo.name = team;
        teamInfo.leader = $"{simpleRegion}{theName}";
        teamInfo.time = latestTime;
        var jt = JsonConvert.SerializeObject(teamInfo);
        request.RawData = Encoding.UTF8.GetBytes(jt);
        request.ConnectTimeout = TimeSpan.FromSeconds(10);
        request.Timeout = TimeSpan.FromSeconds(10);
        request.Send();
    }


    private void Update()
    {
        RefreshTodayScore();
        if (_done) return;
        if (paused) return;
        _timeUsed += Time.deltaTime;
        if (_allCards.Count == 0)
        {
            _minTime = Mathf.Min(_minTime, _timeUsed);
            PlayerPrefs.SetFloat("minTime", _minTime);
            _todayMinTime = Mathf.Min(_todayMinTime, _timeUsed);
            PlayerPrefs.SetFloat("todayMinTime", _todayMinTime);
            PlayerPrefs.SetFloat("latestTime", _timeUsed);
            _done = true;
            uploaded = false;
            _successTimeText.text = $"用时 {_timeUsed:0.00}";
            successPanel.SetActive(true);
            PlayerPrefs.SetInt("passTime", PlayerPrefs.GetInt("passTime", 0) + 1);
            UploadScore();
        }
        else if (_fenceCards.Count == 7 && _backUsed && _undoUsed && _shuffleUsed)
        {
            _done = true;
            _failureTimeText.text = $"用时 {_timeUsed:0.00}";
            failurePanel.SetActive(true);
        }

        timeText.text = $"{_timeUsed:0.00}";
    }

    public void CallAddCardToFence(Card card)
    {
        StartCoroutine(AddCardToFence(card));
    }

    private IEnumerator AddCardToFence(Card card)
    {
        if (_fenceCards.Count >= 7) yield break;
        if (card.inFence) yield break;
        _audioSource.PlayOneShot(collectingClip);
        card.inFence = true;
        var firstSameCard = _fenceCards.LastOrDefault(x => x.value == card.value);
        if (firstSameCard == null) _fenceCards.Add(card);
        else _fenceCards.Insert(_fenceCards.IndexOf(firstSameCard) + 1, card);
        for (var i = 0; i < _fenceCards.Count; i++)
        {
            var fenceCard = _fenceCards[i];
            var position = _fenceZeroPosition + new Vector3(i, 0, 0);
            fenceCard.transform.DOMove(position, .5f);
        }

        var sameCards = _fenceCards.Where(x => x.value == card.value).ToList();
        if (sameCards.Count >= 3)
        {
            foreach (var sameCard in sameCards)
            {
                _allCards.Remove(sameCard);
                _fenceCards.Remove(sameCard);
            }
        }

        yield return new WaitForSeconds(.5f);
        if (sameCards.Count < 3) yield break;

        _audioSource.PlayOneShot(dismissingClip);
        foreach (var sameCard in sameCards)
        {
            sameCard.DestroySelf();
        }

        for (var i = 0; i < _fenceCards.Count; i++)
        {
            var fenceCard = _fenceCards[i];
            var targetPosition = _fenceZeroPosition + new Vector3(i, 0, 0);
            fenceCard.transform.DOMove(targetPosition, .5f);
        }

        yield return new WaitForSeconds(.5f);

        if (_allCards.Count <= 10)
        {
            foreach (var sheep in sheepList)
            {
                sheep.SetActive(true);
            }
        }
    }

    private bool _undoUsed;

    public void UndoOne()
    {
        if (_undoUsed) return;
        _undoUsed = true;

        foreach (var fenceCard in _fenceCards)
        {
            fenceCard.inFence = false;
            fenceCard.transform.DOMove(fenceCard.originalPosition, .5f);
        }

        _fenceCards.Clear();
        undoButton.enabled = false;
        undoButton.GetComponent<Image>().color = Color.black;
    }

    private bool _backUsed;

    public void BackThree()
    {
        if (_backUsed) return;
        _backUsed = true;

        var count = Math.Min(333, _fenceCards.Count);
        for (int i = 0; i < count; i++)
        {
            var card = _fenceCards.First();
            card.inFence = false;
            var targetPosition = _undoZeroPosition + new Vector3(i, 0, 0);
            card.transform.DOMove(targetPosition, .5f);
            _fenceCards.Remove(card);
        }

        backButton.enabled = false;
        backButton.GetComponent<Image>().color = Color.black;
    }

    private IEnumerator DoShuffleAll()
    {
        var valuesList = _allCards.Select(x => x.value).OrderBy(_ => Guid.NewGuid()).ToList();
        for (var i = 0; i < _allCards.Count; i++)
        {
            var card = _allCards[i];
            card.value = valuesList[i];
            card.GetComponentsInChildren<SpriteRenderer>().Last().sprite = cardSprites[card.value - 1];
        }

        _fenceCards = _fenceCards.OrderBy(x => x.value).ToList();
        var fenceValues = _fenceCards.Select(x => x.value).ToHashSetPooled().ToList();
        var removed = false;
        foreach (var fenceValue in fenceValues)
        {
            var sameCards = _fenceCards.Where(x => x.value == fenceValue).ToList();
            if (sameCards.Count < 3) continue;
            removed = true;
            foreach (var sameCard in sameCards)
            {
                _fenceCards.Remove(sameCard);
                _allCards.Remove(sameCard);
                sameCard.DestroySelf();
            }
        }

        if (removed) yield return new WaitForSeconds(.3f);

        for (var i = 0; i < _fenceCards.Count; i++)
        {
            var card = _fenceCards[i];
            var targetPosition = _fenceZeroPosition + new Vector3(i, 0, 0);
            card.transform.DOMove(targetPosition, .5f);
        }
    }

    private bool _shuffleUsed;

    public void ShuffleAll()
    {
        if (_shuffleUsed) return;
        _shuffleUsed = true;
        StartCoroutine(DoShuffleAll());
        shuffleButton.enabled = false;
        shuffleButton.GetComponent<Image>().color = Color.black;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void BackHome()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ToggleMenuPanel()
    {
        menuPanel.SetActive(!menuPanel.activeSelf);
        paused = menuPanel.activeSelf;
    }

    public void ToggleRankPanel()
    {
        rankPanel.SetActive(!rankPanel.activeSelf);
        paused = rankPanel.activeSelf;
    }

    public void TogglePersonPanel()
    {
        personPanel.SetActive(!personPanel.activeSelf);
        paused = personPanel.activeSelf;
    }

    public void ShowRankPanel()
    {
        successPanel.SetActive(false);
        rankPanel.SetActive(true);
    }
}