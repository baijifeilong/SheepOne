using UnityEngine;

public class Sheep : MonoBehaviour
{
    public float speed = 3;
    private int _direction = 1;
    private readonly float _minX = -3.5f;
    private readonly float _maxX = 3.5f;

    private void Update()
    {
        if (transform.position.x <= _minX)
        {
            _direction = 1;
            transform.Rotate(new Vector3(180, 0, 180));
        }

        if (transform.position.x >= _maxX)
        {
            _direction = -1;
            transform.Rotate(new Vector3(180, 0, 180));
        }

        transform.Translate(new Vector3(speed * Time.deltaTime * _direction * Random.Range(0f, 1f), 0, 0), Space.World);
    }

    private void OnCollisionEnter2D()
    {
        GetComponent<Rigidbody2D>().velocity = new Vector2(0, 5) * Random.Range(1f, 1f);
    }
}