using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPanel : MyPanel
{
    protected override void Start()
    {
        base.Start();
        transform.Find("ContentPanel/RestartButton").GetComponent<Button>().onClick
            .AddListener(delegate { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex); });
        transform.Find("ContentPanel/BackButton").GetComponent<Button>().onClick
            .AddListener(delegate { SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex - 1); });
        transform.Find("ContentPanel/QuitButton").GetComponent<Button>().onClick
            .AddListener(Application.Quit);
    }
}