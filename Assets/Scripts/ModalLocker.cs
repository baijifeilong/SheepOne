using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ModalLocker : MonoBehaviour
{
    private Selectable[] _selectables;

    private void OnEnable()
    {
        _selectables = FindObjectsOfType<Selectable>().Where(s => s.interactable && !s.transform.IsChildOf(transform))
            .ToArray();
        foreach (var selectable in _selectables)
        {
            selectable.interactable = false;
        }
    }

    private void OnDisable()
    {
        if (_selectables == null)
            return;

        foreach (var selectable in _selectables)
            selectable.interactable = true;
        _selectables = null;
    }
}