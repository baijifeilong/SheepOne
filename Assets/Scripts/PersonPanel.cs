using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PersonPanel : MyPanel
{
    private Text _bestScoreText;
    private Text _todayScoreText;
    private Text _passTimeText;
    private Dropdown _teamDropDown;
    private Dropdown _regionDropDown;
    private Dropdown _nameDropDown;
    private List<String> _provinces;
    private List<String> _areas;
    private List<String> _names;
    private Button _uploadButton;

    protected override void Start()
    {
        base.Start();
        _names = GameManager.Instance.charNames;
        _todayScoreText = GetComponentsInChildren<Text>()[1];
        _bestScoreText = GetComponentsInChildren<Text>()[2];
        _passTimeText = GetComponentsInChildren<Text>()[3];
        _teamDropDown = GetComponentsInChildren<Dropdown>()[0];
        _regionDropDown = GetComponentsInChildren<Dropdown>()[1];
        _nameDropDown = GetComponentsInChildren<Dropdown>()[2];
        _teamDropDown.ClearOptions();
        _regionDropDown.ClearOptions();
        _nameDropDown.ClearOptions();

        _provinces = GameManager.Instance.AreaDict.Keys.ToList();
        _areas = GameManager.Instance.AreaDict[_provinces[PlayerPrefs.GetInt("teamIndex", 0)]];
        _teamDropDown.AddOptions(_provinces);
        _regionDropDown.AddOptions(_areas);
        _nameDropDown.AddOptions(_names.Select((t, i) => $"{i + 1:000} {t}").ToList());

        _teamDropDown.value = PlayerPrefs.GetInt("teamIndex", 0);
        _regionDropDown.value = PlayerPrefs.GetInt("regionIndex", 0);
        _nameDropDown.value = PlayerPrefs.GetInt("nameIndex", 0);
        _teamDropDown.onValueChanged.AddListener(delegate(int arg0)
        {
            PlayerPrefs.SetInt("teamIndex", arg0);
            _regionDropDown.value = 0;
            _areas = GameManager.Instance.AreaDict[_provinces[arg0]];
            _regionDropDown.ClearOptions();
            _regionDropDown.AddOptions(_areas);
        });
        _regionDropDown.onValueChanged.AddListener(delegate(int arg0) { PlayerPrefs.SetInt("regionIndex", arg0); });
        _nameDropDown.onValueChanged.AddListener(delegate(int arg0) { PlayerPrefs.SetInt("nameIndex", arg0); });
        _uploadButton = GetComponentsInChildren<Button>().Last();
        _uploadButton.onClick.AddListener(GameManager.Instance.UploadScore);
    }

    private void Update()
    {
        var minTime = PlayerPrefs.GetFloat("minTime", 9999.99f);
        var todayMinTime = PlayerPrefs.GetFloat("todayMinTime", 9999.99f);
        var passTime = PlayerPrefs.GetInt("passTime", 0);
        _todayScoreText.text = $"今日最佳: {todayMinTime:0.00}";
        _bestScoreText.text = $"历史最佳: {minTime:0.00}";
        _passTimeText.text = $"通关次数: {passTime}";
    }
}