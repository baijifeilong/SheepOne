using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class Card : MonoBehaviour
{
    public bool inFence;
    public int value;
    private bool _front;
    private SpriteRenderer _parentRenderer;
    private SpriteRenderer _childRender;
    public Vector3 originalPosition;
    public GameObject deathEffectPrefab;

    private void Start()
    {
        _parentRenderer = GetComponent<SpriteRenderer>();
        _childRender = GetComponentsInChildren<SpriteRenderer>().Last();
        _parentRenderer.color = Color.gray;
        _childRender.color = Color.gray;
    }

    private void OnMouseDown()
    {
        if (EventSystem.current.currentSelectedGameObject) return;
        if (!_front) return;
        if (inFence) return;
        transform.DOScale(1, 0.2f);
    }

    private void OnMouseUp()
    {
        if (EventSystem.current.currentSelectedGameObject) return;
        if (!_front) return;
        if (inFence) return;
        transform.DOScale(0.84f, 0.2f);
        GameManager.Instance.CallAddCardToFence(this);
    }

    private void Update()
    {
        var oldFront = _front;
        var hit = Physics2D.Raycast(transform.position, Vector2.zero, 100, 1 << 0);
        _front = hit.collider != null && hit.collider.gameObject == gameObject;
        if (_front == oldFront) return;
        if (_front)
        {
            _parentRenderer.DOColor(Color.white, .3f);
            _childRender.DOColor(Color.white, .3f);
        }
        else
        {
            _parentRenderer.color = Color.gray;
            _childRender.color = Color.gray;
        }
    }

    public void DestroySelf()
    {
        Destroy(gameObject);
        Destroy(Instantiate(deathEffectPrefab, transform.position, Quaternion.identity), 5);
    }
}