using UnityEngine;

public class CameraKeepWidth : MonoBehaviour
{
    private Camera _camera;
    private float _oldOrthographicSize;

    private void Start()
    {
        _camera = GetComponent<Camera>();
        _oldOrthographicSize = _camera.orthographicSize;
    }

    private void Update()
    {
        if (_camera.aspect > 9 / 16f) return;
        _camera.orthographicSize = _oldOrthographicSize / (_camera.aspect / (9 / 16f));
    }
}