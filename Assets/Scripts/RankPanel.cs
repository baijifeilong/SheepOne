using UnityEngine.UI;

public class RankPanel : MyPanel
{
    protected override void Start()
    {
        base.Start();
        RefreshRank();
    }

    private void OnEnable()
    {
        GetComponentInChildren<ScrollRect>().verticalNormalizedPosition = 1;
        GameManager.Instance.RefreshRank();
    }

    private static string SimplifyProvince(string provinceName)
    {
        provinceName = provinceName.Replace("省", "");
        provinceName = provinceName.Replace("市", "");
        provinceName = provinceName.Replace("自治区", "");
        provinceName = provinceName.Replace("壮族", "");
        provinceName = provinceName.Replace("回族", "");
        provinceName = provinceName.Replace("维吾尔", "");
        return provinceName;
    }

    public void RefreshRank()
    {
        var holderTransform = transform.Find("ContentPanel/Scroll View/Viewport/Content");
        for (var i = 0; i < GameManager.Instance.teams.Count; i++)
        {
            var team = GameManager.Instance.teams[i];
            var rankText = holderTransform.GetChild(i).GetComponentsInChildren<Text>()[0];
            var nameText = holderTransform.GetChild(i).GetComponentsInChildren<Text>()[1];
            var scoreText = holderTransform.GetChild(i).GetComponentsInChildren<Text>()[2];
            var personText = holderTransform.GetChild(i).GetComponentsInChildren<Text>()[3];
            var teamText = holderTransform.GetChild(i).GetComponentsInChildren<Text>()[4];
            var rank = (i + 1).ToString().PadRight(2);
            var teamName = (SimplifyProvince(team.name) + "羊队");
            var done = team.done;
            var average = team.average;
            rankText.text = $"#{rank}";
            nameText.text = teamName;
            scoreText.text = $"{team.time:0.00}";
            personText.text = $" 领头羊:  {team.leader}";
            teamText.text = $"通关次数: {done}\n平均用时: {average:0.00}";
        }
    }
}