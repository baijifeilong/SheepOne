using UnityEngine;
using UnityEngine.UI;

public class MyPanel : MonoBehaviour
{
    protected virtual void Start()
    {
        transform.Find("ContentPanel/CloseButton").GetComponent<Button>().onClick
            .AddListener(delegate
            {
                gameObject.SetActive(false);
                GameManager.Instance.paused = false;
            });
    }
}