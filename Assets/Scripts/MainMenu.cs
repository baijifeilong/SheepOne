using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    public void ToggleCheat()
    {
        PlayerPrefs.SetInt("cheat", 1 - PlayerPrefs.GetInt("cheat", 0));
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}